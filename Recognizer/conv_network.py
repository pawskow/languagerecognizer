#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import os
import sys

import numpy as np
import theano
import theano.tensor as T

from theano_conv.mlp import HiddenLayer
from theano_conv.rec_lenet import LeNetConvPoolLayer
from theano_conv.logistic_sgd import LogisticRegression, load_data


DEFAULT_FILTERS = [
                    [[23, 300], [5, 5], [2, 2]],
                    [[9, 148], [5, 5], [2, 2]],
                    [[2, 72], [2, 2], [1, 71]],
                    [[1, 1]]
                    ]


DEFAULT_NKERNS = [1, 6, 12, 15]


def evaluate_lenet5(datasets, classes, resultsFileName, layers_filters=DEFAULT_FILTERS, nkerns=DEFAULT_NKERNS, learning_rate=0.1, n_epochs=500, batch_size=50):

    resultsFile = open(resultsFileName, 'w')

    resultsFile.write("classes = {}, learning_rate = {}, n_epochs = {}, batch_size = {}\n\n".format(classes, learning_rate, n_epochs, batch_size))

    resultsFile.write("liczba warstw: {}\n".format(len(DEFAULT_NKERNS)))

    layerIndex = 0
    for i in DEFAULT_FILTERS:
        if layerIndex < len(DEFAULT_FILTERS) - 1:
            resultsFile.write("warstwa {}: wejscie = {}, okno filtra = {}, pooling = {}, liczba kerneli = {}\n\n"
                              .format(layerIndex, i[0], i[1], i[2], DEFAULT_NKERNS[layerIndex]))
            layerIndex += 1
        else:
            resultsFile.write("warstwa {}: wejscie = {}, liczba kerneli = {}\n\n".format(layerIndex, i[0], DEFAULT_NKERNS[layerIndex]))

    train_set_x, train_set_y = datasets[0]
    valid_set_x, valid_set_y = datasets[1]
    test_set_x, test_set_y = datasets[2]

    n_train_batches = train_set_x.get_value(borrow=True).shape[0]
    n_valid_batches = valid_set_x.get_value(borrow=True).shape[0]
    n_test_batches = test_set_x.get_value(borrow=True).shape[0]
    n_train_batches /= batch_size
    n_valid_batches /= batch_size
    n_test_batches /= batch_size

    # allocate symbolic variables for the data
    index = T.lscalar()  # index to a [mini]batch
    x = T.matrix('x')   # the data is presented as rasterized images
    y = T.ivector('y')  # the labels are presented as 1D vector of
                        # [int] labels
    layer0_input = x.reshape((batch_size, 1, layers_filters[0][0][0], layers_filters[0][0][1]))

    rng = np.random.RandomState(23455)


    layers = []
    layers_count = len(layers_filters)
    layer_input = layer0_input



    for i, layer_filters in enumerate(layers_filters):
        if i<layers_count-1:
            layer = LeNetConvPoolLayer(
                rng,
                input=layer_input,
                image_shape=(batch_size, nkerns[i], layer_filters[0][0], layer_filters[0][1]),
                filter_shape=(nkerns[i+1], nkerns[i], layer_filters[1][0], layer_filters[1][1]),
                poolsize=layer_filters[2]
            )
            layer_input = layer.output
            layers.append(layer)
        else:
            # construct a fully-connected sigmoidal layer
            layer = HiddenLayer(
                rng,
                input=layer_input.flatten(2),
                n_in=nkerns[i] * layer_filters[0][1] * layer_filters[0][1],
                n_out=batch_size,
                activation=T.tanh
            )
            layers.append(layer)
            layer_input = layer.output


    log_regression_layer = LogisticRegression(input=layer_input, n_in=batch_size, n_out=classes)
    layers.append(log_regression_layer)
    cost = log_regression_layer.negative_log_likelihood(y)

    test_model_errors = theano.function(
        [index],
        log_regression_layer.errors(y),
        givens={
            x: test_set_x[index * batch_size: (index + 1) * batch_size],
            y: test_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    test_model_recalls = [

        theano.function(
            [index],
            log_regression_layer.recall(y, clss),
            givens={
                x: test_set_x[index * batch_size: (index + 1) * batch_size],
                y: test_set_y[index * batch_size: (index + 1) * batch_size]
            }
        )
        for clss in range(classes)
        ]

    validate_model = theano.function(
        [index],
        log_regression_layer.errors(y),
        givens={
            x: valid_set_x[index * batch_size: (index + 1) * batch_size],
            y: valid_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    params = None
    for l in layers:
        if params is None:
            params = l.params
        else:
            params = params + l.params

    # create a list of gradients for all model parameters
    grads = T.grad(cost, params)

    # train_model is a function that updates the model parameters by
    # SGD Since this model has many parameters, it would be tedious to
    # manually create an update rule for each model parameter. We thus
    # create the updates list by automatically looping over all
    # (params[i], grads[i]) pairs.
    updates = [
        (param_i, param_i - learning_rate * grad_i)
        for param_i, grad_i in zip(params, grads)
    ]

    train_model = theano.function(
        [index],
        cost,
        updates=updates,
        givens={
            x: train_set_x[index * batch_size: (index + 1) * batch_size],
            y: train_set_y[index * batch_size: (index + 1) * batch_size]
        }
    )

    ###############
    # TRAIN MODEL #
    ###############
    print '... training'
    # early-stopping parameters
    patience = 10000  # look as this many examples regardless
    patience_increase = 2  # wait this much longer when a new best is
                           # found
    improvement_threshold = 0.995  # a relative improvement of this much is
                                   # considered significant
    validation_frequency = min(n_train_batches, patience / 2)
                                  # go through this many
                                  # minibatche before checking the network
                                  # on the validation set; in this case we
                                  # check every epoch

    best_validation_loss = np.inf
    best_iter = 0
    test_score = 0.
    start_time = time.clock()

    epoch = 0
    done_looping = False

    while (epoch < n_epochs) and (not done_looping):
        epoch = epoch + 1
        for minibatch_index in xrange(n_train_batches):

            iter = (epoch - 1) * n_train_batches + minibatch_index

            if iter % 100 == 0:
                print 'training @ iter = ', iter
            cost_ij = train_model(minibatch_index)

            if (iter + 1) % validation_frequency == 0:

                # compute zero-one loss on validation set
                validation_losses = [validate_model(i) for i
                                     in xrange(n_valid_batches)]
                this_validation_loss = np.mean(validation_losses)
                print('epoch %i, minibatch %i/%i, validation error %f %%' %
                      (epoch, minibatch_index + 1, n_train_batches,
                       this_validation_loss * 100.))

                resultsFile.write('epoch %i, minibatch %i/%i, validation error %f %% \n' %
                      (epoch, minibatch_index + 1, n_train_batches,
                       this_validation_loss * 100.))

                # if we got the best validation score until now
                if this_validation_loss < best_validation_loss:

                    #improve patience if loss improvement is good enough
                    if this_validation_loss < best_validation_loss *  \
                       improvement_threshold:
                        patience = max(patience, iter * patience_increase)

                    # save best validation score and iteration number
                    best_validation_loss = this_validation_loss
                    best_iter = iter

                    # test it on the test set
                    test_losses = [
                        test_model_errors(i)
                        for i in xrange(n_test_batches)
                    ]

                    # test it on the test set
                    test_recalls = [
                        [
                            test_model_recalls[cls](i)
                            for i in xrange(n_test_batches)
                        ]
                        for cls in range(classes)
                    ]
                    test_score = np.mean(test_losses)
                    test_score_precision = 1 - test_score
                    test_score_recall = np.mean([np.mean(test_recall) for test_recall in test_recalls])
                    print(('     epoch %i, minibatch %i/%i, test recall of '
                           'best model %f and precision of best model %f %%') %
                          (epoch, minibatch_index + 1, n_train_batches,
                           test_score_recall * 100., test_score_precision * 100.))

                    resultsFile.write(('     epoch %i, minibatch %i/%i, test recall of '
                           'best model %f and precision of best model %f %% \n') %
                          (epoch, minibatch_index + 1, n_train_batches,
                           test_score_recall * 100., test_score_precision * 100.))

            if patience <= iter:
                done_looping = True
                break

    end_time = time.clock()
    print('\nOptimization complete.')
    print('Best validation score of %f %% obtained at iteration %i, '
          'with test recall %f and precision %f %%' %
          (best_validation_loss * 100., best_iter + 1, test_score_recall * 100., test_score_precision * 100.))
    print >> sys.stderr, ('The code for file ' +
                          os.path.split(__file__)[1] +
                          ' ran for %.2fm' % ((end_time - start_time) / 60.))

    resultsFile.write('Optimization complete.\n')
    resultsFile.write('Best validation score of %f %% obtained at iteration %i, '
          'with test recall %f and precision %f %% \n' %
          (best_validation_loss * 100., best_iter + 1, test_score_recall * 100., test_score_precision * 100.))
    resultsFile.write('The code for file ' +
          os.path.split(__file__)[1] +
          ' ran for %.2fm \n' % ((end_time - start_time) / 60.))

    resultsFile.close()

if __name__ == '__main__':
    resultsFileName = sys.argv[1]

    a = load_data("../samples")
    evaluate_lenet5(a, 3, resultsFileName)
