#!/usr/bin/env python
# -*- coding: utf-8 -*-

from features import mfcc
from features import logfbank
import scipy.io.wavfile as wav
import numpy as np
import os
from random import shuffle

SAMPLES_LENGTH = 3.01 #sec
EXTENSION = '.wav'

def generate_samples_from_dir(dir_name):
    for f_name in os.listdir(dir_name):
        if f_name.endswith(EXTENSION):
            for sample in generate_samples(os.path.join(dir_name, f_name)):
                yield sample

def scale_linear_bycolumn(rawpoints, high=1.0, low=0.0):
    mins = np.min(rawpoints, axis=0)
    maxs = np.max(rawpoints, axis=0)
    rng = maxs - mins
    return high - (((high - low) * (maxs - rawpoints)) / rng)

def generate_samples(file_path):
    (rate,sig) = wav.read(file_path)

    signal_length = sig.shape[0]
    step = SAMPLES_LENGTH * rate
    beginning = 0

    normal_end = signal_length - step
    while beginning <= normal_end:
        yield mfcc(sig[beginning:beginning+step], rate, numcep=23)
        beginning = beginning + step


    yield mfcc(sig[-step:], rate, numcep=23)

def generate_dataset(samples_path):
    training_set = []
    validation_set = []
    test_set = []

    lang_enum = {'de': 0, 'it': 1, 'SpeechCorpus': 2}

    for language_directory in next(os.walk(samples_path))[1]:
        if str(language_directory) in lang_enum:
            training_samples = generate_samples_from_dir(os.path.join(samples_path, language_directory, "training"))

            for sample in training_samples:
                sample = sample.flatten()
                training_set.append((sample, lang_enum[str(language_directory)]))

            validation_samples = generate_samples_from_dir(os.path.join(samples_path, language_directory, "validation"))

            for sample in validation_samples:
                sample = sample.flatten()
                validation_set.append((sample, lang_enum[str(language_directory)]))

            test_samples = generate_samples_from_dir(os.path.join(samples_path, language_directory, "test"))

            for sample in test_samples:
                sample = sample.flatten()
                test_set.append((sample, lang_enum[str(language_directory)]))

    shuffle(training_set)
    shuffle(validation_set)
    shuffle(test_set)

    result = ((np.array([x[0] for x in training_set]), np.array([x[1] for x in training_set])),
              (np.array([x[0] for x in validation_set]), np.array([x[1] for x in validation_set])),
              (np.array([x[0] for x in test_set]), np.array([x[1] for x in test_set])))

    return result