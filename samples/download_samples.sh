#!/bin/bash

for i in "SpeechCorpus" "de" "it"
do
	wget -r --no-parent --reject "index.html*" http://www.repository.voxforge1.org/downloads/$i/Trunk/Audio/Main/16kHz_16bit/

	#wget -r --no-parent --reject "index.html*" http://www.repository.voxforge1.org/downloads/$i/Trunk/Audio/Original/44.1kHz_16bit/

	mkdir $i
	cd $i

	find .. -name '*.tgz' -exec tar zxf '{}' \;
	rm -rf "../www.repository.voxforge1.org"

	find . -mindepth 2 -name '*.wav' -exec mv -- '{}' . \;
	find . -mindepth 1 -maxdepth 1 -type d -exec rm -rf '{}' \;

	cd ..
done


