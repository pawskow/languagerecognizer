#!/bin/bash

for dir in $(find . -mindepth 1 -maxdepth 1 -type d -exec echo {} \;)
do
	cd $dir

	mkdir training

	for i in $(seq 1 900)
	do
		files=(*)

		f=${files[RANDOM % ${#files[@]}]}
		while [ -d "$f" ] || [ $(echo " $(soxi -D "$f") < 4.0 " | bc) -eq 1 ] ; do
			f=${files[RANDOM % ${#files[@]}]}
		done

		mv "$f" training/
	done

	mkdir validation

	for i in $(seq 1 100)
	do
		files=(*)

		f=${files[RANDOM % ${#files[@]}]}
		while [ -d "$f" ] || [ $(echo " $(soxi -D "$f") < 4.0 " | bc) -eq 1 ] ; do
			f=${files[RANDOM % ${#files[@]}]}
		done

		mv "$f" validation/
	done

	mkdir "test"

	for i in $(seq 1 100)
	do
		files=(*)

		f=${files[RANDOM % ${#files[@]}]}
		while [ -d "$f" ] || [ $(echo " $(soxi -D "$f") < 4.0 " | bc) -eq 1 ] ; do
			f=${files[RANDOM % ${#files[@]}]}
		done

		mv "$f" test/
	done

	cp training/* .
	cp validation/* .
	cp test/* .

	cd ..
done


