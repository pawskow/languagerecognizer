#!/bin/bash

for dir in $(find . -mindepth 1 -maxdepth 1 -type d -exec echo {} \;)
do
	cd $dir

	rm -r training
	rm -r validation
	rm -r "test"

	cd ..
done
